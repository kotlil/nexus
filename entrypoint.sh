#!/bin/sh
#entrypoint.sh
set -e
(
newpsswd=$NEWPSSWD #env var
url=$URL #env var
nexus=1 #if 1- while is looping
echo -n "checking status of nexus..." >> /logs/entrypoint.log
while [ "$nexus" = "1" ]

do

if curl --output /dev/null --silent --head --fail $url
   then
      defpsswd=`cat /nexus-data/admin.password` #read default password
      echo ": nexus is online!" >> /logs/cyklus.log
      echo "URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd" >> /logs/cyklus.log

	#  /#admin/system/api
	# change admin password

      curl -u admin:$defpsswd -X PUT "$url/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"

      nexus=0 #stop cyklus
   else
      echo -n "." >> /logs/cyklus.log
   fi
done
)&

exec "${SONATYPE_DIR}/start-nexus-repository-manager.sh"
