entrypoint.sh
is needed for start all the processes in the background

how to build:
```docker build -t kotlil .```

examples:

```docker run -d -p 8081:8081 -e "newpsswd=test" --name kotlil kotlil:latest```

```docker run -d -p 8081:8081 -e "newpsswd=test" -v nexus-data:/nexus-data/ --name kotlil kotlil:latest```

volumes:

```docker run -d -p 8081:8081 -v $(pwd)/logs:/logs --name kotlil50 kotlil:latest```

```docker exec -it a02406297e2e cat /logs/psswd.log```

finall run with new admin password and volume:

```docker run -d -p 8081:8081 -e "newpsswd=test" -v $(pwd)/logs:/logs --name kotlil kotlil:latest```

```docker run -d -p 8081:8081 -e NEWPSSWD='test' -e URL='localhost:8081' -v $(pwd)/logs:/logs --name kotlilx kotlil:latest```

or you can use finall dockerimage:
https://hub.docker.com/repository/docker/kotlil/nexus
