#!/bin/sh
containerid=$1
newpsswd=$2
url="127.0.0.1:8081"
nexus=1
echo -n "checking status of nexus..."
while [ "$nexus" = "1" ]

do

if curl --output /dev/null --silent --head --fail $url
   then
      defpsswd=`docker exec -it "$containerid" cat /nexus-data/admin.password` #read default password
      echo ": nexus is online!"
      echo "URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd"
      echo "URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd" >> ./logs/test.sh
      ##### change admin password #####
      curl -u admin:$defpsswd -X PUT "$url/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"
      nexus=0
   else
      echo -n "."
   fi
done
