#until.sh
#(
containerid=$1
newpsswd=$2
url="http://localhost:8081"
echo "log1" >> ./logs/log
echo "until.sh URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd" >> ./logs/log

until curl --output /dev/null --silent --head --fail $url; do

defpsswd=`docker exec -it "$containerid" cat /nexus-data/admin.password`
  printf '.'
  echo "until.sh URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd" >> ./logs/log

 curl -u admin:$defpsswd -X PUT "http://localhost:8081/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"

sleep 5
done
#)&
