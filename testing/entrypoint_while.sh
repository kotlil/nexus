#!/bin/sh
#entrypoint_while.sh
set -e

#./changeadminpassword.sh

(
newpsswd=$1
url="http://localhost:8081"
echo "newpsswd: $newpsswd url: $url" >> /logs/change.log
while curl --output /dev/null --silent --head --fail $url; do
echo "cyklus $newpsswd $url" >> /logs/change.log
defpsswd=`cat /nexus-data/admin.password`
echo "defpswws $defpsswd"
 curl -u admin:$defpsswd -X PUT "$url/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"
sleep 5

done
)&


exec "${SONATYPE_DIR}/start-nexus-repository-manager.sh"

