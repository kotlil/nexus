#!/bin/sh

containerid=$1
newpsswd=$2

defpsswd=`docker exec -it $containerid cat /nexus-data/admin.password`

wait-on -t 5000 http-get://localhost:8081/ && echo "default: $defpsswd new: $newpsswd" && 
curl -u admin:$defpsswd -X PUT "http://localhost:8081/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"

