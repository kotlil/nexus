#!/bin/sh
#if.sh

containerid=$1
newpsswd=$2

defpsswd=`docker exec -it "$containerid" cat /nexus-data/admin.password`

url="http://localhost:8081"
if curl --output /dev/null --silent --head --fail "$url"; then
  echo "URL exists: $url, defpsswd: $defpsswd, newpsswd: $newpsswd" && curl -u admin:$defpsswd -X PUT "http://localhost:8081/service/rest/beta/security/users/admin/change-password" -H  "accept: application/json" -H  "Content-Type: text/plain" -d "$newpsswd"
else
  echo "URL does not exist: $url"
fi
