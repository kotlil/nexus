FROM sonatype/nexus3

#ENV NEWPSSWD=$NEWPSSWD
ENV SONATYPE_DIR=/opt/sonatype
COPY --chown=nexus:nexus ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]
#CMD [ "sh", "-c", "${SONATYPE_DIR}/start-nexus-repository-manager.sh" ]
#CMD [ "sh", "-c", "${NEWPSSWD}" ] 
